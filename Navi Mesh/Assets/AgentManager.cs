﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AgentManager : MonoBehaviour
{
    GameObject[] agents;
    
    // Start is called before the first frame update
    void Start()
    {
        agents = GameObject.FindGameObjectsWithTag("AI");
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetMouseButtonDown(0))
        {
            RaycastHit ray;

            if(Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out ray, 1000))
            {
                foreach(GameObject ai in agents)
                {
                    ai.GetComponent<AIControl>().agent.SetDestination(ray.point);
                }
            }
        }
    }
}
