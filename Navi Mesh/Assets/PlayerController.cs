﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public Vector3 offSet;
    public Vector3 threshold;
    public GameObject player;

    public float speed;

    // Start is called before the first frame update
    void Start()
    {
        player = this.GetComponent<GameObject>();
    }

    // Update is called once per frame
    void Update()
    {
        float x = Input.GetAxis("Horizontal");

        float z = Input.GetAxis("Vertical");

        transform.Translate((x * speed) * Time.deltaTime, 0, (z * speed) * Time.deltaTime);
    }
}
