﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class AIControl : MonoBehaviour
{
    NavMeshAgent agent;

    GameObject[] goalLocation;

    float detectionRadius = 20;
    float fleeRadius = 10;

    // Start is called before the first frame update
    void Start()
    {
        goalLocation = GameObject.FindGameObjectsWithTag("goal");
        agent = this.GetComponent<NavMeshAgent>();

        agent.SetDestination(goalLocation[Random.Range(0, goalLocation.Length)].transform.position);
        ResetAgent();
    }

    // Update is called once per frame
    void Update()
    {
        if(agent.remainingDistance<1)
        {
            ResetAgent();
            agent.SetDestination(goalLocation[Random.Range(0, goalLocation.Length)].transform.position);
        }
    }

    void ResetAgent()
    {
        float speedMult = Random.Range(0.5f, 1.5f);
        agent.speed = 2 * speedMult;
        agent.angularSpeed = 100;
        agent.ResetPath();
    }

    public void DetectNewObstacle(Vector3 location)
    {
        if(Vector3.Distance(location,this.transform.position)<detectionRadius)
        {
            Vector3 fleeDirection = (this.transform.position - location).normalized;
            Vector3 newGoal = this.transform.position + fleeDirection * fleeRadius;

            NavMeshPath path = new NavMeshPath();
            agent.CalculatePath(newGoal, path);

            if(path.status != NavMeshPathStatus.PathInvalid)
            {
                agent.SetDestination(path.corners[path.corners.Length - 1]);
                agent.speed = 10;
                agent.angularSpeed = 500;
            }
        }
    }
}
