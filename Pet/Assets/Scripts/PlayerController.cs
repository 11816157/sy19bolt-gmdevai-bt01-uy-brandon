﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    private Rigidbody playerCube;
    private float speed = 3.75f;

    Vector3 mover;

    void Awake()
    {
        playerCube = this.GetComponent<Rigidbody>();
    }

    void Update()
    {
        mover = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
    }

    void FixedUpdate()
    {
        playerCube.MovePosition(transform.position+(mover*speed*Time.fixedDeltaTime));
    }
}
