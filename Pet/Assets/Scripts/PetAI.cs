﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PetAI : MonoBehaviour
{
    private Rigidbody pet;
    private float smoothSpeed = 1.5f;
    public Transform character;
    public Vector3 followingCharacter;

    void Awake()
    {
        pet = this.GetComponent<Rigidbody>();
    }
    
    void FixedUpdate()
    {
        Vector3 newPosition = character.position + (character.rotation * followingCharacter);
        Vector3 smoothMovement = Vector3.Slerp(transform.position, newPosition, smoothSpeed*Time.fixedDeltaTime);
        transform.position = smoothMovement;

        transform.LookAt(character);
    }
}
